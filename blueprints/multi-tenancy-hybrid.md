# Multi-Tenancy and hybrid cluster through Host API

With BareMetalPool we proposed an approach to multi-tenancy by directly
virtualizing the BMC. Another approach is to only virtualize part of
the API offered by the BareMetalHost offering only access to the configuration
of the image, the boot configuration (userData, networkData and metaData) and
the administrative status of the server (online or not).

We introduce a new Host custom resource. A host can be bound to a BareMetalHost.
Its specification is very similar to a Metal3Machine but its status is closer
to a BareMetalHost. Its role is to decouple the link between Metal3Machine and
BareMetalHost.


# The Host resource

We inherit the HostSelector from the Metal3Machine and we expose the following
fields from the BareMetalHost:

* Online
* Image
* UserData
* NetworkData
* MetaData

We can also expose Architecture but rather as a matching field complementing
the hostSelector.

There are some fields we do not expose :

* BootMode: if the BareMetalHost could be easily coerced to change boot mode
  at the BMC level. This field could be given to the user but it is not clear
  that this can be done. The BootMode currently reflect the status of the
  server rather than it controls it.
* AutomatedCleaningMode: As those machines are meant to be reused, it makes
  sense to keep this flag under the control of the infrastructure.

## Binding between Host and BareMetalHost

When there is no image provided and the host offline, the binding MUST be
broken. The host is no more associated to a BareMetalHost and the BareMetalHost
should be available for another binding.

To ensure host reuse, we must give an explicit hint somewhere that prevents the
reuse. It is not clear if the behaviour implemented by capm3 can be directly
reused or if we need some adaptation to abstract node-reuse api on the host.

As there is no namespace restriction, we may want a mechanism to restrict the use
of a BareMetalHost to one or several namespaces.

## Host and BareMetalHost metadata

We must synchronize Host and BareMetalHost metadata up to a certain level:

* BareMetalHost metadata can contain information necessary for the Metal3 data
  template mechanism.
* Some mechanisms may use metadata to signal operations over the BareMetalHost.
  For example rebooting/pause are done through annotations.
* Some meta-data are important at a given level. For example to keep information
  on some bindings. They do not need to be synchronized on the other side but must
  be kept.

To keep the flows consistent with an algorithmic that is not prone to race problems,
we set the following rules:

* The normal flow of meta-data is from the BareMetalHost to the Host.
  Only a small set of well known fields are treated in the opposite direction: they
  SHOULD be set in a reserved domain (action.host.kanod.io) to avoid conflicts 
  with the normal propagation.
* When a label or an annotation are copied from the BareMetalHost to the Host, their
  names are recorded in two annotations on the Host. When a name is those list is
  no more associated to a label or an annotation on the BareMetalHost, the name must
  be removed from the list annotation and the label or annotation on the Host must
  be deleted.

## Controller implementation

The implementation should reuse as much as possible the Metal3Machine code
as long as it is not directly linked with the cluster.

The main difference with the Metal3Machine is that the goal is to have
separated namespaces for BareMetalHost and Host. This means that secrets
are copied from the Host namespace to the BareMetalHost namespace.

## Retargeting the cluster-api-provider-metal3

Rather than targetting bareMetalHost directly, the metal3 provider could
generate Host. We propose to implement it either:

* as an option of the provider.
* when a specific annotation is put on the Cluster object.

# Implementing Hybrid clusters with Host

What is exposed by the host API could be fullfilled by different compute
resources. A Kubevirt VirtualMachine, an Openstack VM could be bound to a host
exposing the right selector. This would extend the scope of the retargeted
cluster-api-provider as it could now create clusters with different underlying
implementations of machine deployment. All the templating resources, ipam
management offered by Metal3 would be directly available.

## Operating with several Host controllers
A specific label that we will call "kind" identifies the target of the host.
This label is specified in the host selector field as an exact matching label
(no support is planned for match expressions).

Each controller MUST respond to only a specific value of the label and MUST
cancel reconciliation for other values. For example the value "baremetal"
would be reserved for the Host controller implementing the standard Metal 3
logic.

### Implementation Remark
It is clear that using an entry in a map which is a subfield of the specification
of the Host is sub-optimal when controllers during the reconciliation loop must
decide whether they handle or not resource. It seems reasonable that an 
admission controller ensure the value is more readily available:

* the value of the entry is copied by to a specific field kind in
  the specification of the host
* it is also coppied as a label so that controller can ask the apiServer to filter
  only relevant hosts (requirement that may be lifted if KEP4359 is implemented)

## Supplying values to controllers
With disposable target resources, it is necessary to find a way to specify the
size of the resource (number of vcpu, memory and disk size, connected networks etc.) 
and sometimes credentials to create the resource.

We can use again the hostSelector field to specify specific labels. To make the
contract clear, all those labels should be in a well identified domain and
probably the same as kind. But the name and the semantic of those label constraints
would be controller specific.

# Pivoting

This is the main limitation of the proposal. When and if pivoting is used, we
cannot move the bareMetalHost with the Host. This means:
* that we must be able to proxy the link and replace a direct access to a
  BareMetalHost by a proxied version.
* As Ironic is not moved, the initial cluster cannot be deleted.

Note: a metal3 machine was declared owner of the baremetalhost it was linked to,
to enable the automatic move of the resource. This ownership link cannot
(different namespaces) and must not (no move of compute resource) exists.

The capability of moving a BareMetalHost from one Ironic deployment to another
should be kept but it would fall outside the scope of capi to become an operation
specific to baremetal host. In those cases, a Redfish proxy virtualizing the
access to the BMC would still make sense.

## Remote Host
A remote host is a regular host resource marked with a resource to access another
namespace potentially in another cluster. The remote host specification
must be synchronized with a copied resource in the target namespace. 
The status of the host in the target namespace must be synchronized back to the 
remote host. 

Meta-data must be synchronized in the same direction as between BareMetalHost
and Host: from the remote copy to the original host except for the reserved
domain. We use the same mechanism to identify the deleted labels/annotations
(and those lists annotations must not be synchronized).

## Remote Host controller

The remote host is seen as a special kind with a label specifying
the real kind of the host (the one used on the remote cluster).
Another label points to a secret giving the configuration to
access the remote cluster.

The labelSelector match specifying that the host is remote must be removed and replaced by the real kind on the target cluster.

## Companion resources
Hosts are associated with other resources. Those resources must be
available on the remote cluster and must be transfered/synchronized
with the Host. There are up to three secrets to transfer coresponding
to the following fields:

* userData
* metaData
* networkData

On the remote cluster, we can only use the namespace used for the Host.
We will use a deterministic naming on the remote cluster.

This mechanism is unfortunately restricted to well known fields. Other
resources that could be linked through labels will not be transmitted
In the use case the need arised for OpenStack, but in that case, we do not need remote host to do the pivot. We could not handle additional
resources for Kubevirt if we had to.

## Proxy Implementation
Even if it seems easy to use a service account in the target namespace and
give its long life token as credential for the remote host, the approach has
several drawbacks:

* using long life service tokens is frowned upon and Kubernetes documentation
  recomends to implement one owns authentication logic for such use cases.
* building the service account and granting the right roles on the fly would
  mean that the controller building such object has very elevated RBAC privileges. The fact that we need to manage secrets makes the approach even trickier.

A better approach is to have a dedicated proxy that could be exposed either as a standard service or using K8S API aggregation layer to reuse the cluster endpoint. The proxy answers to request from the remote host controller to create/modify/delete
target host and their associated secrets. It will watch the target host and send back notification to the remote host.

The proxy must at least implement the following actions:
* create, get, update, delete for hosts in the target namespace
* watch the host in the target namespace
* create, update, delete for secrets associated to host with a specific role.
It should probably also implement some kind of credential rotation. It probably needs also
a host list for global reconciliation.

The controller on the source identifies the target host with a label containing the namespace and
name of the host on the origin cluster. When a watch/list is performed, only this key is exchanged.

## Authentication
Authentication and access control at the level of the proxy is very important to limit the scope of the request to authorized users.
In the demonstration we use basic authentication over https. Another approach would be to use client certificates.

## Creating the credentials
A resource handled by the proxy could be used to create credentials in a given
namespace. The resulting secret could be passed to the namespaces targeting
that namespace. Using specific secret types, it should be easy to ensure that
the mechanism cannot be abused to manage arbitrary secrets in another namespace.

The namespace must be dedicated to Hosts and their associated secrets
from a single source. Otherwise there can be conflicts:
* on secrets from other uses
* between hosts if it is the target from different remotes.

# Quotas
With a system giving access to shared compute resources from several namespaces
reprenting tenants, it is necessary to implement a mechanism to limit the compute
resources managed by each namespace.

An equivalent of quotas for pods could be defined for hosts. A HostQuota would
specify in a namespace the maximum number of Host in that namespace along
several dimensions:

* it should be able to count the number of host having a specific label requested
* it should be able to count the number of host with a specific value for a given
  label (eg. kind=baremetalhost)
* it should be able to aggregate the values of labels containing parsable integer
  values and impose a limit on the aggregated value (eg for memory size).

Note: the mechanism will not work well with hostSelector matchExpression, so it
is only good for selectors on the reserved domains (kind, controller specific
resources, etc.)

Remote host should probably be excluded from the count. The access control should be done
in the remote namespace (TODO: understand better the implications).

Non goals: trading resources limits between namespaces.
