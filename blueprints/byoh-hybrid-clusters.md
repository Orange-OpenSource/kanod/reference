# Goal

Describe the layout of an hybrid cluster (a cluster with multiple sources of compute like bare-metal, kubevirt VM, VM in an Openstack cloud, etc.) and the architecture of the solution implementing those clusters.

We will use Cluster API with Bring Your Own Host, an infrastructure provider developped by VMWare. The provider uses compute resources that are already started and that register with the Cluster life-cycle manager through an agent (host-agent) launched on the compute resource.

# Context

## Sub resources involved
Cluster API transforms a compute resource in a Kubernetes node that can join a Kubernetes cluster.

A node can be described by the following characteristics:

1. The hardware resource on which it is run (Example: a specific bareMetal node, a VM with specific network attachment or PCI passthrough to specific hardware)
2. The image that is deployed on this hardware resource (typically a Qcow2 image)
3. The configuration of this image (The bulk of the cloud-init configuration)
4. The configuration of the kubelet itself.

Points 2 and 3 are usually performed together: an OS image requires a configuration and the configuration is dependent on the OS image. Because of the immutable infrastructure approach taken by Cluster API, reconfiguring an OS image usually does not make sense and a new image is used but the two steps could be more cleanly separated with a truly immutable OS.

With an infrastructure provider like Metal3, the last two steps are joined in the cloud-init configuration as the Kubelet configuration is just another configuration file but with BYOH (Bring your own host) provider, the Kubelet configuration is provided through the host-agent a component that registers the host as available for Cluster API.

As a result, a host may be deprovisionned through the host agent and redeployed with an updated configuration without being rebooted.

## Taxonomy of compute resources
There are two distinct kind of compute resources that have slightly different life-cycles:

* disposable resources: typically VM. When they are not needed, they are completely discarded.
* reusable resources: a baremetal server cannot be disposed but it may be reused by another cluster.

A reusable resource requires a resource scheduler. In Metal3, the Metal3Machine controller is the scheduler and the BareMetalHost is the actual compute resource.

With Bring Your Own Host, every resource must first be activated, so even dispoable resources are turned into reusable resource (BYOHost). The BYOMachine controller is the scheduler. The difference in the approach may appear if only the kubelet configuration is changed as Byoh may perform a rescheduling without allocating new compute resources.

## Bring Your Own Host Life Cycle

* We assume that BYOH CRD and controllers are deployed on the Life Cycle Manager (LCM)
* A cluster description using BYO is loaded on the LCM. BYO machines are spawned waiting for
  compliant BYOHost resources to appear.
* Using a BootstrapKubeconfig CR, for each compute resource, a service account with limited capabilities is created on the LCM.
* A compute resource is launched with a Host Agent component that contains the Kubeconfig obtained from the BootstrapKubeconfig
* The Host Agent registers on the LCM a BYOHost custom resource. The BYOHost is annotated by the Host-Agent with labels representing capabilities of the host.
* The BYOMachine controller links BYOMachine with BYOHost using the labels as selectors to find compliant compute resources. The Kubelet is then launched on the compute resource and joins the cluster.

## Existing resources

### BareMetalPool

A BareMetalPool represents a scalable set of BareMetalHost (ie bare-metal servers) fullfilling a set of constraints. Those constraints are expressed in the custom resource through metadata (labels and annotations).

BareMetalPool were explicitly introduced in the modelling of BareMetal clusters because they represented another clustering of resources that could be distinct from the traditional layout around machine deployment / control-plane especially regarding networking. But even though it is still not clear that we could not assign one distinct pool per machine deployment or control-plane
without loss of flexibility.

### Bmh-Data

BmhData is an equivalent of DataTemplate but at the level of the compute resource instead of the machine. It was initially designed to compute and link the secret of a network configuration for the preprovisioning of a BareMetalHost. But the current version is
* not limited to preprovisioning: it also handle userData and networkData
* not limited to BareMetalHost as it has been extended to KubeVirt machine.
A better name would be ComputeTemplate.

The resource must be associated to the compute resource by a specific annotation on the compute resource containing the name of the BmhData resource. Both objects must be in the same namespace. Usually a template is associated to several compute ressources, for example all the BareMetalHost of a BareMetalPool.

The BmhData points to a secret containing a template. The template is expanded with information coming from various sources:
* from the metadata of the compute resource (either labels or annotations)
* from specific fields in the object (the boot Mac address).
* from specific sources that are generated for the specific instance of the compute resource:
   * Ip address from IpPools associated to the BmhData but this behavior is now deprecated as request for address can also be defined at the level of the BareMetalHost through annotations.
   * a BootstrapKubeconfig typically used to register the compute resource with BYOH controllers.

An alternate design would be to have a truly generic template mechanism that would handle only the first source of data. It would impose a few constraints:
* it would be impossible to establish the link, so the name of the generated secret would have to be constrained (for example the concatenation of the names of the template and the compute resource).  The idea would be to accept a pointer to a non yet existing secret in the compute resource. The controller of the compute resource must then stop processing cleanly until the resource is available.
* Only information from metadata can be handled. It probably means that the bootMacAddress must be  copied as an annotation on the BareMetalHost.
* The template must probably contain a field giving the kind of the compute resource for which the expansion is done and the name of label used by the resource to identify the ComputeTemplate (but not the domain of the label that should be fixed).
* Special care must be taken to ensure that the compute resource has full control over the secrets generated. That is why the domain of labels pointing to ComputeTemplate must be constrained.

With this design there would be a distinct template for each generated resource (userData, networkData, preprovisioningNetworkData).

# Proposed expansion of a Kanod cluster for BYOH

## Requirements

### Expected Workflow Structure

We want to keep the deployment workflow as similar as possible to the existing Kanod workflow with Metal3. In particular we would like to avoid explicit new intermediate resources in the description of the cluster even if they may be necessary during the deployment.

### Structure of the Cluster Description

We keep the standard structure of the cluster :
* One description of the control-plane and several machine deployments describing groups of similar worker nodes.
* Each description is organized around:
  * an image with a given kubernetes version
  * a ServerConfig specifying the image specialization
  * a set of parameters targetting the kubelet configuration
  * a control-plane configuration.

Although it would be nice to have a clear separation between control-plane and image configuration. It is important to note that configuration files refered by control-plane containers need to be supplied in the image configuration.

## Proposed Implementation
### A new ByohHostPool for Reusable Compute Resources.

A ByoHostPool is a scalable resource that tries to deploy a given userData and a given OS image over a set of well identified compute resource fullfilling a set of metadata constraints. The userData is carried by a template (Bmh-data/ComputeTemplate). The userData will also contain the BootstrapKubeconfig and a label that uniquely identifies the origin of the BYOHost generated.

Each ComputeTemplate is uniquely identified by a hash of its content, the ByohHostPool that inherits the hash of the ComputeTemplate must also combine it with a unique reference to the image. The label must then be added to the ServerConfig so that the ByohHost holds this label (note: obviously the label definition is not part of the hash).

*It is the element that will be used by the BYOMachine to identify the host to use.*

All the mechanics to compute and assign the right hash at the right place will be handled by the kanod-updater.

### Simplified workflow for Disposable Resources.
A scalable compute resource is associated to each machineTemplate. For example a VirtualMachinePool for Kubevirt. A ComputeTemplate must be assigned to the machines declared. It must respect the same conditions on the hash label as the one for ByohHostPool.

### Autoscaling
Two levels of autoscalers are used:
* Hardware requirements are handled by the baremetalPool autoscaler.
* OS Image and configuration requirements are handled by a new autoscaler for either ByoHostPool or the scalable resources associated to disposable compute resource.

The second autoscaler is mandatory because it maintains the right level of resources during a cluster upgrade. There is a one to one mapping between machine sets and the pools. The logic behind the autoscaling is fairly generic and should not depend on the kind of the scaled pool.

The first autoscaler is useful to limit the number of hardware resources required.

### Remarks
In the current workflow, there is no need to associate images or userData at the level of the BareMetalPool.The semantics during upgrades was never clear. So it is recommended to remove those fields from the BareMetalPool custom resource.

The BYOHostPool is not strictly linked with a BareMetalPool. It rather relie on BareMetalHost metadata to identify compliant servers.
