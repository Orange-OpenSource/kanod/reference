#  Copyright (C) 2024 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

# The code is a Python version of
# https://github.com/moeriki/json-schema-flatten
# (C) 2016 Dieter Luypaert

import argparse
import copy
import yaml

DEF_KEY = 'definitions'
REF_KEY = '$ref'
TYPE_KEY = 'type'
ITEMS_KEY = 'items'
PROP_KEY = 'properties'
REF_PREFIX = '#/definitions/'
ARRAY_TYPE = 'array'
OBJECT_TYPE = 'object'


def is_object(obj):
    return type(obj) == dict


def is_array(obj):
    return type(obj) == list


def is_JSONSchema(obj):
    return (
        type(obj) == dict and
        ((obj.get(TYPE_KEY, '') == OBJECT_TYPE and
          is_object(obj.get(PROP_KEY, None))) or
         (obj.get(TYPE_KEY, '') == ARRAY_TYPE and
          is_object(obj.get(ITEMS_KEY, None)) and
          (TYPE_KEY not in obj[ITEMS_KEY] or 
           obj[ITEMS_KEY][TYPE_KEY] in [ARRAY_TYPE, OBJECT_TYPE])
          )))


def flatten(schema_origin):
    new_schema = copy.deepcopy(schema_origin)
    definitions = new_schema.get(DEF_KEY, {})
    if DEF_KEY in new_schema:
        del new_schema[DEF_KEY]

    def free_definition_name(name):
        new_name = name
        idx = 1
        while new_name in definitions:
            new_name = f'{name}_{idx}'
            idx += 1
        return new_name

    def crawl(obj, base_path, ref_redirects):
        if REF_KEY in obj:
            target = obj[REF_KEY]
            if target.startswith(REF_PREFIX):
                new_target = ref_redirects.get(target[len(REF_PREFIX):], None)
                if new_target is not None:
                    obj[REF_KEY] = REF_PREFIX + new_target
            return

        for (key, prop) in obj.items():
            if is_JSONSchema(prop):
                ref_path = free_definition_name(
                    base_path + key.capitalize() if len(base_path) > 0
                    else key)
                if prop.get(TYPE_KEY, '') == OBJECT_TYPE:
                    definitions[ref_path] = prop
                    obj[key] = {REF_KEY: REF_PREFIX + ref_path}
                elif prop.get(TYPE_KEY, '') == ARRAY_TYPE:
                    definitions[ref_path] = prop[ITEMS_KEY]
                    prop[ITEMS_KEY] = {REF_KEY: REF_PREFIX + ref_path}

                if DEF_KEY in prop:
                    ref_redirects = {**ref_redirects}
                    for (prop_def_name, prop_def) in prop[DEF_KEY].items():
                        prefixed_prop_def_name = free_definition_name(
                            ref_path + 'Definition' +
                            prop_def_name.capitalize())
                        definitions[prefixed_prop_def_name] = prop_def
                        ref_redirects[prop_def_name] = prefixed_prop_def_name
                        crawl(definitions[prefixed_prop_def_name],
                              prefixed_prop_def_name,
                              ref_redirects)
                    del prop[DEF_KEY]
                crawl(definitions[ref_path], ref_path, ref_redirects)
            elif is_object(prop):
                crawl(prop, base_path, ref_redirects)

    for (existing_definition_name, existing_definition) in definitions:
        crawl(existing_definition, existing_definition_name, {})
    crawl(new_schema, '', {})
    if len(definitions) > 0:
        new_schema[DEF_KEY] = definitions
    return new_schema


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help='Input file')
    parser.add_argument('-o', '--output', help='Output file')
    parser.add_argument('-p', '--path', help='Path in input')
    args = parser.parse_args()
    with open(args.input, 'r') as fd:
        schema = yaml.safe_load(fd)
    if args.path:
        for segment in args.path.split('/'):
            if is_object(schema):
                schema = schema.get(segment, None)
                if schema is None:
                    print(f'cannot find segment {segment} of {args.path}')
                    exit(1)
            elif is_array(schema):
                try:
                    idx = int(segment)
                except ValueError:
                    print(f'cannot parse segment {segment}'
                          'as integer for indexing array')
                try:
                    schema = schema[idx]
                except IndexError:
                    print(f'invalid index {idx} in path {args.path}')
    new_schema = flatten(schema)
    with open(args.output, 'w') as fd:
        yaml.dump(new_schema, fd)


main()
