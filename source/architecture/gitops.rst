.. _gitops:
======
GitOps
======

Git-Ops principles
------------------
Git Ops is a term coined by Alexis Richardson from Weaveworks. The key
principle is to use a *source control management* system (a git repository)
to hold the *wished* state of a system.

One of the branch is selected as representing the consecutive sequence of
states of the system. To modify the state, the operators should submit their
change as *pull requests* that can be validated by whatever process (reviews,
tests in CI chains) and merged.

Because we need to represent the state of the system, gitops implies a
declarative approach to system description and to be truly declarative it
implies an *immutable infrastructure* approach to ensure that what is deployed
has not drifted from the wished state through a long, continuous but imperfect
path of changes.

Immutable infrastructure means that component evolution goes through deletion
of old instance and creation of new instance with the updated code from
start rather than modification of an existing object.

Cluster Co construction
-----------------------

One of the innovation of Kanod is that it offers a way to mix two sources
of truth to build the infrastructure:

* the infrastructure manager defines the parameters of the basic blocks,
  for example most of the network parameters
* the cluster administrator selects among the available nodes, the kind and
  the number of nodes and the Kubernetes parameters such as the Kubernetes 
  version or the container engine.

We use a single cluster description language. The infrastructure manager
defines templates and controls what can be overriden by the cluster
administrator. The result of the synthesis process is stored in a snapshot
git that holds the actual cluster api manifests that are deployed on the
management cluster. Each branch in the snapshot cluster is the audit trail
of a single cluster.

Target cluster manifests
------------------------
Finally the synthesis process produces also manifests that are deployed on the
target cluster. The main purpose is to help the deployment of core middleware
components that relie on information from the infrastructure.



