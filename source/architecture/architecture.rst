==================
Kanod architecture
==================

Goal of Kanod
=============

Kanod is a framework to launch several Kubernetes clusters on groups of servers.
It distinguishes three kind of actors:

* The infrastructure administrator who manages the hardware, controls the access
  to the resources and define the rules around the network usage
* cluster administrators who are the client of the Kanod framework. They create
  Kubernetes clusters fitting their needs and request resources from the
  infrastructure
* Developers who create templates for the nodes launched on the cluster. Kanod
  provides basic default templates but they can be extended to accomodate the
  specific requirements of the available hardware or the feature expected from
  the node.

Kanod presents a declarative API to end users. Nodes characteristics are
described in Yaml. For each kind of node an OpenAPI schema is given. Clusters
are described as assemblies of nodes following Cluster API structure which is
the underlying framework of Kanod. All descriptions are stored in Git servers
following the Git-Ops principles. The underlying software is ArgoCD that
synchronizes Kubernetes clusters with the contents of git repositories.

Main components
===============

To fulfil its function, Kanod is organized around four kind of servers:

* the life-cycle manager which is the core of the Kanod.
* a git server holding server descriptions, cluster definitions and the status
  of those definitions in the infrastructure.
* a repository is used to store container images, operating system images
  and Kubernetes manifests. It is provisionned by the developers CICD pipelines
  and consumed by the life-cycle manager and the clusters.
* Security sensitive assets are stored in a vault server.

Finally clusters may need persistent storage provided by external storage
clusters like Ceph clusters. The management of persistent storage is outside the
scope of this document. Note that ClusterAPI strategy for node update is not
very well-suited for hyperconverged storage.

.. figure:: images/Kanod.png
    :figwidth: 80%
    :align: center

    Kanod Architecture

Kanod in a bottle
=================

*Kanod in a bottle* is a prototypical deployment of Kanod on a single machine
using virtual machines to simulate real hardware servers. In the following we
will use the abbreviation KIAB for *Kanod in a bottle*.
KIAB releases use git branches to offer stable versions of Kanod at different stage
of development. *Kanod in a bottle* versions are named using star names.

KIAB provides OS images and configurations for:

* The life-cycle manager itself
* An artifact repository that provides OS and container images. The artifact
  repository uses a Nexus Repository Manager from Sonatype.
* A git server used by the infrastructure manager and the cluster managers to
  host their configurations. We use GOGS (Go Git Server).
* An optional secured stoarage for credentials. It uses an Hashicorp Vault server.

The configuration of the last server is neither secured nor highly available
and is only provided as an example or for use in a CICD chain.
We recommend the use of an indepedently deployed Vault infrastructure in
production.

