.. _clusterapi:

===========
Cluster API
===========

`Cluster API <https://github.com/kubernetes-sigs/cluster-api>`_ is a
Kubernetes sub-project focused on providing declarative
APIs and tooling to simplify provisioning, upgrading, and operating multiple
Kubernetes clusters. Cluster API is a project of Kubernetes
*SIG Cluster life-cycle*.

Cluster API defines custom resources that represent the configuration of a
Kubernetes *target cluster* and of the infrastructure that supports it.
The life-cycle of a Kubernetes cluster can be fully automated within a
*management cluster*.
After the initial bootstrap, the management cluster can even be the target
cluster itself.

Cluster API distinguishes:

* a bootstrap provider that automates the provisioning of a cluster on an
  existing infrastructure. It focuses on **kubeadm** a project of *SIG Cluster
  life-cycle*.
* an infrastructure provider that automates the configuration of the underlying
  infrastructure. This can be virtual machines in a cloud provider (public or
  private) or bare-metal servers.

Kanod focuses on bare-metal Kubernetes clusters and uses **Metal³** as
infrastructure provider.

Metal³
------
Metal³ is a CNCF sandbox project that provides a declarative API for
bare metal host management. It can be viewed as a pair of operator:

* an operator that manages bare metal host
* a cluster-api infrastructure provider that uses those bare metal hosts
  as underlying resources for bare-metal kubernetes clusters.

Cluster API Custom Resources
----------------------------
We only give a brief overview of Cluster API custom resources and refer the
reader to the `Cluster API Book <https://cluster-api.sigs.k8s.io/>`_

Cluster
  A Kubernetes cluster viewed as a resource in Kubernetes. It mainly specifies
  the entry point and is used as a owner resource by the other components.

KubeAdmControlPlane
  A full description of the configuration of the control-plane of the cluster
  with pointers to the underlying machine templates.

MachineSet
  A set of machines

MachineDeployment
  A machine set with life-cycle management (as in Deployment vs ReplicaSet).
  It is only used for worker nodes.

Machine
  A resource on which a Kubernetes node can be created

KubeAdmConfig
  The declarative configuration of a worker node (a machine member of a
  machine deployment)

A lot of those resources have counter-parts in Metal³: Metal3Cluster,
Metal3Machine.

There are also resources specific to Metal³ that exploits cloud-init
structure and templating capabilities: **Metal3DataTemplate** that can be
used to transform resource labels and annotations (notably of the actual
bare metal hosts) in cloudinit meta-data and **Metal3MachineTemplate** that
define selectors for baremetal hosts and where the link with meta-data
template is established.
