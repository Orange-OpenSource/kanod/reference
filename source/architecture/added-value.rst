=================
Kanod added value
=================

Based on Cluster-API
====================
Kanod benefits from the combination of Cluster-API and GitOps:

* The configuration of every infrastructure component can be captured by a
  declarative API. Having a uniform API even for the low level configuration
  of operating systems ensure that we can develop tools to manage the split
  of responsabilities between different actors. It would be less obvious if
  we handled imperative recipes rather than declarative manifests.
* Kubernetes is not only a container orchestrator, it is the resource
  orchestrator and its scope slowly evolves to become a kind of distributed
  operating system.
* Immutable infrastructure ensure deterministic operation. It reduces the risk
  that the actual state of systems drifts away from the wished configuration.

.. _decl-os:

Declarative OS Configuration
============================
Even the low level configuration of the node OS is made declarative. We rely
on an extension/replacement of cloud-init that makes it extensible. It comes 
hand in hand with a framework for diskimage-builder that incorporates the
extensions of cloud-init in each element.

* Declarative OS configuration hides accessory differences between OS.
  Configuration of core services (clock, CRI, container repositories) is
  the same for Centos, Ubuntu or OpenSuse.
* Configuration is described with Json schemas. It is possible to validate
  statically OS configuration syntax. Semantic validation could be added in
  the future.
* Kanod provides a uniform framework to inject secrets in such configurations.
  The framework relies on Vault and configuration manifests contain only
  pointers to the secret stored in the Vault component. It is also possible
  to leverage the TPM of the baremetal servers to make the secrets of a given
  cluster only accessible to the servers hosting it. Even the life-cycle
  manager cannot directly access those secrets.

The main components used in this approach are

* :doc:`Image Building <../image-builder/index>` for providing an extensible framework for OS images.
* :doc:`Kanod Registrar <../registrar/index>` for the integration of TPM

.. _bmaas:

Baremetal as a Service
======================
As soon as one wants to define several clusters belonging to different tenants
on a single pool of servers, the limitation of real hardware compared to
virtualized infrastructures appear:

* Servers can only have one owner. It is not easy to trade servers between
  different tenants.
* By default connectivity between the servers should be a full mesh. Adding
  isolation between the servers of different tenants requires a reconfiguration
  of the underlying network.

Kanod introduces the notion of bare-metal pools. It is a new scalable
custom ressource that can be used to create ephemeral BareMetalHost from
constraints and a template. The underlying infrastructure select real servers
fulfilling the constraints and assigns temporary credentials to access those
servers. Two implementations are provided:

* A Redfish proxy that translates the requests with temporary credentials to
  requests on the real hardware
* An oprator that creates temporary accounts on the servers with reduced
  privileges.

Those pools are annotated with network configuration. An operator will 
interact with top of rack switches to configure the access to the relevant
virtual networks (Today, only VLANs are supported).

For both server allocation and network configuration Kanod uses two separate
of resources that can be hosted on different clusters:

* User level custom resources that represent requests on the infrastructure.
* Underlying low level resources used to fulfil user level requests.

Low level resources are associated with the real credentials to access the
resources and should never be exposed to end-users. On the other hand, high
level user resources can be safely pivoted as long as they stay under the
control of the cluster administrator as they contain user credentials.

The main components are:

* :doc:`BaremetalPool Custom Resource<../pools/baremetalpool/index>` which defines a contract to get a set
  of ``BaremetalHost`` custom resources from a remote provider.
* :doc:`Broker Resources <../pools/brokerdef/index>` with two implementations of such a provider
* :doc:`Network Custom Resource <../pools/network-operator/index>` which provides operators and resources to handle
  the networking part.

.. _compute_aas:

Compute as a Service
====================

Rather than completely virtualize the server through its BMC, we can also virtualize
the functions that are useful to define a workload. A Host custom resource defines
a schedulable workload defined by the OS image and the OS configuration (cloud-init).
A host selector field let the user defines the characteristic of the compute resource
that will execute the workload. The status of the resource reflects the status of the
underlying server.

Using Hosts requires a modification of the cluster api provider for bare metal (capm3)
but it brings many benefits: not only we can manage multi-tenancy but we can also build
hybrid clusters where hosts are implemented as virtual machines in different kind
of clouds.

* :doc:`Host Custom Resource<../hosts/host-operator/index>` which defines the generic Host
  resource and its implementation for bare-metal servers.

.. _synthesis:

GitOps Orchestration
====================
The description of a cluster in Kanod is split in two parts:

* The infrastructure administrator describes a set of template containing
  node configurations.
* The cluster administrator assembles templates and override part of the 
  definitions to build a cluster fulfilling his needs.

A typical split of responsability is to let the infrastructure specify most
of the networking configuration and let the user choose the OS distribution
and the Kubernetes version.

ClusterDef is the custom resource used by the infrastructure administrator to
describe a new cluster under the responsabiility of a cluster administrator.
It contains a description of the Git repository used by the cluster administrator
to define the cluster, a pointer to a set of templates and some parameters
specific to this cluster but under the responsability of the infrastructure
administrator.

ClusterDef is also used to orchestrate the upgrade of clusters. A change
of Kubernetes version will trigger a rollout begining with the control-plane
and propagating to all the worker deployments. The upgrade request 
may skip over intermediate versions: ClusterDef will follow an upgrade path
specified by the infrastructure administrator to go through the required
intermediate versions without manual intervention from the end-users.

The main components are:

* :doc:`ClusterDef Custom Resource <../clusterdef/index>` which coordinates the life-cycle.
* :doc:`The Kanod updater plugin <../updater/index>` with the kanod-updater plugin which translate a high
  level description provided by both administrators in a set of cluster-api 
  resources.


