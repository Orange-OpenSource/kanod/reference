.. define a hard line break for HTML
.. |br| raw:: html

   <br />


*************
API Reference
*************

- :ref:`bmp.kanod.io/v1<k8s-api-bmp-kanod-io-v1>`
- :ref:`broker.kanod.io/v1<k8s-api-broker-kanod-io-v1>`
- :ref:`brokernet.kanod.io/v1<k8s-api-brokernet-kanod-io-v1>`
- :ref:`config.kanod.io/v1<k8s-api-config-kanod-io-v1>`
- :ref:`gitops.kanod.io/v1alpha1<k8s-api-gitops-kanod-io-v1alpha1>`
- :ref:`netpool.kanod.io/v1<k8s-api-netpool-kanod-io-v1>`
- :ref:`network.kanod.io/v1alpha1<k8s-api-network-kanod-io-v1alpha1>`
- :ref:`network.kanod.io/v1beta1<k8s-api-network-kanod-io-v1beta1>`


.. _k8s-api-bmp-kanod-io-v1:

bmp.kanod.io/v1
===============


Package v1 contains API Schema definitions for the bmp v1 API group


Resource Types
--------------

- :ref:`BareMetalPool<k8s-api-kanod-io-baremetalpool-v1-baremetalpool>`
- :ref:`BareMetalPoolList<k8s-api-kanod-io-baremetalpool-v1-baremetalpoollist>`



.. _k8s-api-kanod-io-baremetalpool-v1-baremetalpool:

BareMetalPool
-------------




BareMetalPool is the Schema for the baremetalpools API

*Appears in:*


* :ref:`BareMetalPoolList<k8s-api-kanod-io-baremetalpool-v1-baremetalpoollist>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``apiVersion`` *string*
     - ``bmp.kanod.io/v1``
   * - ``kind`` *string*
     - ``BareMetalPool``
   * - ``metadata`` `ObjectMeta <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/#objectmeta-v1-meta>`_
     - Refer to Kubernetes API documentation for fields of `metadata`.
   * - ``spec`` :ref:`BareMetalPoolSpec<k8s-api-kanod-io-baremetalpool-v1-baremetalpoolspec>`
     - 
   * - ``status`` :ref:`BareMetalPoolStatus<k8s-api-kanod-io-baremetalpool-v1-baremetalpoolstatus>`
     - 

.. _k8s-api-kanod-io-baremetalpool-v1-baremetalpoollist:

BareMetalPoolList
-----------------




BareMetalPoolList contains a list of BareMetalPool



.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``apiVersion`` *string*
     - ``bmp.kanod.io/v1``
   * - ``kind`` *string*
     - ``BareMetalPoolList``
   * - ``metadata`` `ListMeta <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/#listmeta-v1-meta>`_
     - Refer to Kubernetes API documentation for fields of `metadata`.
   * - ``items`` :ref:`BareMetalPool<k8s-api-kanod-io-baremetalpool-v1-baremetalpool>` array
     - **mandatory** |br| 

.. _k8s-api-kanod-io-baremetalpool-v1-baremetalpoolspec:

BareMetalPoolSpec
-----------------




BareMetalPoolSpec defines the desired state of BareMetalPool

*Appears in:*


* :ref:`BareMetalPool<k8s-api-kanod-io-baremetalpool-v1-baremetalpool>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``replicas`` *integer*
     - Number of baremetals requested
   * - ``poolName`` *string*
     - **mandatory** |br| Name of the pool delivering baremetal access
   * - ``credentialName`` *string*
     - **mandatory** |br| Name of the secret associated with the pool
   * - ``address`` *string*
     - **mandatory** |br| Address of the redfish broker
   * - ``labelSelector`` map [*string*] *string*
     - label for the selection of the baremetal which will be associated with the pool
   * - ``maxServers`` *integer*
     - Number max of baremetal which can be associated with the pool a negative value means no limit. 0 disable the pool.
   * - ``redfishSchema`` *string*
     - **mandatory** |br| Schema used for BMC url with Refish
   * - ``brokerConfig`` *string*
     - **mandatory** |br| BrokerConfig is the name of the configmap containing the broker CA
   * - ``bmhLabels`` map [*string*] *string*
     - Labels assigned to the baremetalhost created by the baremetalpool
   * - ``bmhAnnotations`` map [*string*] *string*
     - Annotations assigned to the baremetalhost created by the baremetalpool
   * - ``blockingAnnotations`` *string* array
     - BlockingAnnotations contains a list of annotations which keeps the barematalhosts paused

.. _k8s-api-kanod-io-baremetalpool-v1-baremetalpoolstatus:

BareMetalPoolStatus
-------------------




BareMetalPoolStatus defines the observed state of BareMetalPool

*Appears in:*


* :ref:`BareMetalPool<k8s-api-kanod-io-baremetalpool-v1-baremetalpool>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``bmhnameslist`` *string* array
     - List of the baremetalhost available
   * - ``status`` *string*
     - **mandatory** |br| Status of the pool
   * - ``replicas`` *integer*
     - **mandatory** |br| Number of baremetalhost available


.. _k8s-api-broker-kanod-io-v1:

broker.kanod.io/v1
==================


Package v1 contains API Schema definitions for the broker v1 API group


Resource Types
--------------

- :ref:`BareMetalDef<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-baremetaldef>`
- :ref:`BareMetalDefList<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-baremetaldeflist>`
- :ref:`PoolDef<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-pooldef>`
- :ref:`PoolDefList<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-pooldeflist>`
- :ref:`PoolUser<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-pooluser>`
- :ref:`PoolUserList<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-pooluserlist>`



.. _k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-baremetaldef:

BareMetalDef
------------






*Appears in:*


* :ref:`BareMetalDefList<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-baremetaldeflist>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``apiVersion`` *string*
     - ``broker.kanod.io/v1``
   * - ``kind`` *string*
     - ``BareMetalDef``
   * - ``metadata`` `ObjectMeta <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/#objectmeta-v1-meta>`_
     - Refer to Kubernetes API documentation for fields of `metadata`.
   * - ``spec`` :ref:`BareMetalDefSpec<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-baremetaldefspec>`
     - 
   * - ``status`` :ref:`BareMetalDefStatus<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-baremetaldefstatus>`
     - 

.. _k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-baremetaldeflist:

BareMetalDefList
----------------




BareMetalDefList contains a list of BareMetalDef



.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``apiVersion`` *string*
     - ``broker.kanod.io/v1``
   * - ``kind`` *string*
     - ``BareMetalDefList``
   * - ``metadata`` `ListMeta <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/#listmeta-v1-meta>`_
     - Refer to Kubernetes API documentation for fields of `metadata`.
   * - ``items`` :ref:`BareMetalDef<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-baremetaldef>` array
     - **mandatory** |br| 

.. _k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-baremetaldefspec:

BareMetalDefSpec
----------------




BareMetalDefSpec defines the desired state of BareMetalDef

*Appears in:*


* :ref:`BareMetalDef<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-baremetaldef>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``id`` *string*
     - **mandatory** |br| Id of the baremetal
   * - ``poolDef`` *string*
     - Name of pooldef booking the baremetal or empty
   * - ``netDefMap`` map [*string*] *string*
     - List of network bindings
   * - ``url`` *string*
     - **mandatory** |br| BMC address of the baremetal
   * - ``baremetalcredential`` *string*
     - **mandatory** |br| Name of the secret storing the BMC credentials of the baremetal
   * - ``macAddress`` *string*
     - **mandatory** |br| MAC address of the baremetal
   * - ``k8slabels`` map [*string*] *string*
     - K8S labels which will be assigned to the baremetalhost created for this baremetal
   * - ``k8sannotations`` map [*string*] *string*
     - K8S annotations which will be assigned to the baremetalhost created for this baremetal
   * - ``rootDeviceHints`` `RootDeviceHints <https://github.com/metal3-io/baremetal-operator/blob/main/docs/api.md#rootdevicehints>`_
     - DeviceName value which will be assigned to the baremetalhost created for this baremetal (for example : ``sda`` for real baremetal, ``vda`` for libvirt VM)
   * - ``disablecertificateverification`` *boolean*
     - **mandatory** |br| DisableCertificateVerification value which will be assigned to the baremetalhost created for this baremetal

.. _k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-baremetaldefstatus:

BareMetalDefStatus
------------------




BareMetalDefStatus defines the observed state of BareMetalDef

*Appears in:*


* :ref:`BareMetalDef<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-baremetaldef>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``state`` *string*
     - **mandatory** |br| Status of the BareMetalDef

.. _k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-pooldef:

PoolDef
-------






*Appears in:*


* :ref:`PoolDefList<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-pooldeflist>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``apiVersion`` *string*
     - ``broker.kanod.io/v1``
   * - ``kind`` *string*
     - ``PoolDef``
   * - ``metadata`` `ObjectMeta <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/#objectmeta-v1-meta>`_
     - Refer to Kubernetes API documentation for fields of `metadata`.
   * - ``spec`` :ref:`PoolDefSpec<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-pooldefspec>`
     - 
   * - ``status`` :ref:`PoolDefStatus<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-pooldefstatus>`
     - 

.. _k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-pooldeflist:

PoolDefList
-----------




PoolDefList contains a list of PoolDef



.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``apiVersion`` *string*
     - ``broker.kanod.io/v1``
   * - ``kind`` *string*
     - ``PoolDefList``
   * - ``metadata`` `ListMeta <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/#listmeta-v1-meta>`_
     - Refer to Kubernetes API documentation for fields of `metadata`.
   * - ``items`` :ref:`PoolDef<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-pooldef>` array
     - **mandatory** |br| 

.. _k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-pooldefspec:

PoolDefSpec
-----------




PoolDefSpec defines the desired state of PoolDef

*Appears in:*


* :ref:`PoolDef<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-pooldef>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``username`` *string*
     - **mandatory** |br| Name of the user associated with the pool
   * - ``labelSelector`` map [*string*] *string*
     - label for the selection of the baremetal which will be associated with the pool
   * - ``maxServers`` *integer*
     - Number max of baremetal which can be associated with the pool a negative value means no limit. 0 disable the pool.
   * - ``netDefMap`` map [*string*] *string*
     - NetworkDefinition list

.. _k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-pooldefstatus:

PoolDefStatus
-------------




PoolDefStatus defines the observed state of PoolDef

*Appears in:*


* :ref:`PoolDef<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-pooldef>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``state`` *string*
     - **mandatory** |br| Status of the PoolDef
   * - ``networkdefinitions`` *string* array
     - 

.. _k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-poollimit:

PoolLimit
---------






*Appears in:*


* :ref:`PoolUserSpec<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-pooluserspec>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``label`` *string*
     - **mandatory** |br| Label is the label on the baremetal host that is checked
   * - ``value`` *string*
     - The machine is counted if the label has this value. If not specified, it is the presence of the label that is counted.
   * - ``max`` *integer*
     - **mandatory** |br| Max is the maximum number of machines with this configuration for this user.

.. _k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-poolusage:

PoolUsage
---------






*Appears in:*


* :ref:`PoolUserStatus<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-pooluserstatus>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``label`` *string*
     - **mandatory** |br| Label is the label on the baremetal host that is checked
   * - ``value`` *string*
     - The machine is counted if the label has this value. If not specified It is the presence of the label that is counted.
   * - ``current`` *integer*
     - **mandatory** |br| Current is the current number of machines with this configuration used by this user.

.. _k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-pooluser:

PoolUser
--------




PoolUser is the Schema for the poolusers API

*Appears in:*


* :ref:`PoolUserList<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-pooluserlist>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``apiVersion`` *string*
     - ``broker.kanod.io/v1``
   * - ``kind`` *string*
     - ``PoolUser``
   * - ``metadata`` `ObjectMeta <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/#objectmeta-v1-meta>`_
     - Refer to Kubernetes API documentation for fields of `metadata`.
   * - ``spec`` :ref:`PoolUserSpec<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-pooluserspec>`
     - 
   * - ``status`` :ref:`PoolUserStatus<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-pooluserstatus>`
     - 

.. _k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-pooluserlist:

PoolUserList
------------




PoolUserList contains a list of PoolUser



.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``apiVersion`` *string*
     - ``broker.kanod.io/v1``
   * - ``kind`` *string*
     - ``PoolUserList``
   * - ``metadata`` `ListMeta <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/#listmeta-v1-meta>`_
     - Refer to Kubernetes API documentation for fields of `metadata`.
   * - ``items`` :ref:`PoolUser<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-pooluser>` array
     - **mandatory** |br| 

.. _k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-pooluserspec:

PoolUserSpec
------------




PoolUserSpec defines the desired state of PoolUser

*Appears in:*


* :ref:`PoolUser<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-pooluser>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``credentialName`` *string*
     - **mandatory** |br| CredentialName is the name of the secret containing the password associated with the pool user
   * - ``maxServers`` *integer*
     - MaxServers it the maximum number of servers that can be registered for this user
   * - ``limits`` :ref:`PoolLimit<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-poollimit>` array
     - Limits imposed on the resources attached to this user.

.. _k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-pooluserstatus:

PoolUserStatus
--------------




PoolUserStatus defines the observed state of PoolUser

*Appears in:*


* :ref:`PoolUser<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-pooluser>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``used`` *integer*
     - **mandatory** |br| 
   * - ``usage`` :ref:`PoolUsage<k8s-api-gitlab-com-orange-opensource-kanod-brokerdef-api-v1-poolusage>` array
     - **mandatory** |br| 


.. _k8s-api-brokernet-kanod-io-v1:

brokernet.kanod.io/v1
=====================


Package v1 contains API Schema definitions for the brokernet v1 API group


Resource Types
--------------

- :ref:`NetworkAttachment<k8s-api-kanod-io-brokernet-v1-networkattachment>`
- :ref:`NetworkAttachmentList<k8s-api-kanod-io-brokernet-v1-networkattachmentlist>`
- :ref:`NetworkDefinition<k8s-api-kanod-io-brokernet-v1-networkdefinition>`
- :ref:`NetworkDefinitionList<k8s-api-kanod-io-brokernet-v1-networkdefinitionlist>`
- :ref:`Switch<k8s-api-kanod-io-brokernet-v1-switch>`
- :ref:`SwitchList<k8s-api-kanod-io-brokernet-v1-switchlist>`



.. _k8s-api-kanod-io-brokernet-v1-dhcpmode:

DhcpMode
--------


*Underlying type:* *string*



*Appears in:*


* :ref:`NetworkDefinitionSpec<k8s-api-kanod-io-brokernet-v1-networkdefinitionspec>`



.. _k8s-api-kanod-io-brokernet-v1-linksspec:

LinksSpec
---------






*Appears in:*


* :ref:`SwitchSpec<k8s-api-kanod-io-brokernet-v1-switchspec>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``switches`` :ref:`SwitchInfoSpec<k8s-api-kanod-io-brokernet-v1-switchinfospec>` array
     - **mandatory** |br| Switches

.. _k8s-api-kanod-io-brokernet-v1-networkattachment:

NetworkAttachment
-----------------




NetworkAttachment is the Schema for the networkattachments API

*Appears in:*


* :ref:`NetworkAttachmentList<k8s-api-kanod-io-brokernet-v1-networkattachmentlist>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``apiVersion`` *string*
     - ``brokernet.kanod.io/v1``
   * - ``kind`` *string*
     - ``NetworkAttachment``
   * - ``metadata`` `ObjectMeta <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/#objectmeta-v1-meta>`_
     - Refer to Kubernetes API documentation for fields of `metadata`.
   * - ``spec`` :ref:`NetworkAttachmentSpec<k8s-api-kanod-io-brokernet-v1-networkattachmentspec>`
     - 
   * - ``status`` :ref:`NetworkAttachmentStatus<k8s-api-kanod-io-brokernet-v1-networkattachmentstatus>`
     - 

.. _k8s-api-kanod-io-brokernet-v1-networkattachmentlist:

NetworkAttachmentList
---------------------




NetworkAttachmentList contains a list of NetworkAttachment



.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``apiVersion`` *string*
     - ``brokernet.kanod.io/v1``
   * - ``kind`` *string*
     - ``NetworkAttachmentList``
   * - ``metadata`` `ListMeta <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/#listmeta-v1-meta>`_
     - Refer to Kubernetes API documentation for fields of `metadata`.
   * - ``items`` :ref:`NetworkAttachment<k8s-api-kanod-io-brokernet-v1-networkattachment>` array
     - **mandatory** |br| 

.. _k8s-api-kanod-io-brokernet-v1-networkattachmentspec:

NetworkAttachmentSpec
---------------------




NetworkAttachmentSpec defines the desired state of Networkattachment

*Appears in:*


* :ref:`NetworkAttachment<k8s-api-kanod-io-brokernet-v1-networkattachment>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``hostName`` *string*
     - **mandatory** |br| Name of the host
   * - ``hostInterface`` *string*
     - **mandatory** |br| Physical network interface on the host
   * - ``switchName`` *string*
     - **mandatory** |br| Switch name linked to the host
   * - ``switchPort`` *string*
     - **mandatory** |br| Switch port connected to the host
   * - ``networkDefinitions`` map [*string*] *string*
     - List of NetworkDefinitions

.. _k8s-api-kanod-io-brokernet-v1-networkattachmentstatus:

NetworkAttachmentStatus
-----------------------




NetworkAttachmentStatus defines the observed state of Networkattachment

*Appears in:*


* :ref:`NetworkAttachment<k8s-api-kanod-io-brokernet-v1-networkattachment>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``state`` *string*
     - 

.. _k8s-api-kanod-io-brokernet-v1-networkdefinition:

NetworkDefinition
-----------------




NetworkDefinition is the Schema for the NetworkDefinitions API

*Appears in:*


* :ref:`NetworkDefinitionList<k8s-api-kanod-io-brokernet-v1-networkdefinitionlist>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``apiVersion`` *string*
     - ``brokernet.kanod.io/v1``
   * - ``kind`` *string*
     - ``NetworkDefinition``
   * - ``metadata`` `ObjectMeta <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/#objectmeta-v1-meta>`_
     - Refer to Kubernetes API documentation for fields of `metadata`.
   * - ``spec`` :ref:`NetworkDefinitionSpec<k8s-api-kanod-io-brokernet-v1-networkdefinitionspec>`
     - 
   * - ``status`` :ref:`NetworkDefinitionStatus<k8s-api-kanod-io-brokernet-v1-networkdefinitionstatus>`
     - 

.. _k8s-api-kanod-io-brokernet-v1-networkdefinitionlist:

NetworkDefinitionList
---------------------




NetworkDefinitionList contains a list of NetworkDefinition



.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``apiVersion`` *string*
     - ``brokernet.kanod.io/v1``
   * - ``kind`` *string*
     - ``NetworkDefinitionList``
   * - ``metadata`` `ListMeta <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/#listmeta-v1-meta>`_
     - Refer to Kubernetes API documentation for fields of `metadata`.
   * - ``items`` :ref:`NetworkDefinition<k8s-api-kanod-io-brokernet-v1-networkdefinition>` array
     - **mandatory** |br| 

.. _k8s-api-kanod-io-brokernet-v1-networkdefinitionspec:

NetworkDefinitionSpec
---------------------




NetworkDefinitionSpec defines the desired state of NetworkDefinition

*Appears in:*


* :ref:`NetworkDefinition<k8s-api-kanod-io-brokernet-v1-networkdefinition>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``vnid`` *string*
     - **mandatory** |br| Virtual network identifier
   * - ``type`` *string*
     - **mandatory** |br| Virtual network type
   * - ``subnetPrefix`` *string*
     - **mandatory** |br| Subnet IP/mask address
   * - ``subnetStart`` *string*
     - **mandatory** |br| Subnet range start
   * - ``subnetEnd`` *string*
     - **mandatory** |br| Subnet range end
   * - ``dhcpMode`` :ref:`DhcpMode<k8s-api-kanod-io-brokernet-v1-dhcpmode>`
     - **mandatory** |br| DhcpMode
   * - ``domainNameServers`` *string* array
     - DomainNameServers
   * - ``gatewayIp`` *string*
     - Gateway IP/mask address
   * - ``dhcpServerIp`` *string*
     - **mandatory** |br| Subnet DHCP server address
   * - ``credentialName`` *string*
     - **mandatory** |br| Name of the secret associated to this network definition

.. _k8s-api-kanod-io-brokernet-v1-networkdefinitionstatus:

NetworkDefinitionStatus
-----------------------




NetworkDefinitionStatus defines the observed state of NetworkDefinition

*Appears in:*


* :ref:`NetworkDefinition<k8s-api-kanod-io-brokernet-v1-networkdefinition>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``state`` *string*
     - **mandatory** |br| Status

.. _k8s-api-kanod-io-brokernet-v1-switch:

Switch
------




Switch is the Schema for the switches API

*Appears in:*


* :ref:`SwitchList<k8s-api-kanod-io-brokernet-v1-switchlist>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``apiVersion`` *string*
     - ``brokernet.kanod.io/v1``
   * - ``kind`` *string*
     - ``Switch``
   * - ``metadata`` `ObjectMeta <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/#objectmeta-v1-meta>`_
     - Refer to Kubernetes API documentation for fields of `metadata`.
   * - ``spec`` :ref:`SwitchSpec<k8s-api-kanod-io-brokernet-v1-switchspec>`
     - 
   * - ``status`` :ref:`SwitchStatus<k8s-api-kanod-io-brokernet-v1-switchstatus>`
     - 

.. _k8s-api-kanod-io-brokernet-v1-switchinfospec:

SwitchInfoSpec
--------------






*Appears in:*


* :ref:`LinksSpec<k8s-api-kanod-io-brokernet-v1-linksspec>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``name`` *string*
     - Name
   * - ``port`` *string*
     - Port

.. _k8s-api-kanod-io-brokernet-v1-switchlist:

SwitchList
----------




SwitchList contains a list of Switch



.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``apiVersion`` *string*
     - ``brokernet.kanod.io/v1``
   * - ``kind`` *string*
     - ``SwitchList``
   * - ``metadata`` `ListMeta <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/#listmeta-v1-meta>`_
     - Refer to Kubernetes API documentation for fields of `metadata`.
   * - ``items`` :ref:`Switch<k8s-api-kanod-io-brokernet-v1-switch>` array
     - **mandatory** |br| 

.. _k8s-api-kanod-io-brokernet-v1-switchspec:

SwitchSpec
----------




SwitchSpec defines the desired state of Switch

*Appears in:*


* :ref:`Switch<k8s-api-kanod-io-brokernet-v1-switch>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``name`` *string*
     - **mandatory** |br| Switch name
   * - ``vendor`` *string*
     - Switch vendor information
   * - ``ipAddress`` *string*
     - **mandatory** |br| IP address of the management interface
   * - ``credentials`` *string*
     - **mandatory** |br| Name of the secret associated to this switch
   * - ``links`` :ref:`LinksSpec<k8s-api-kanod-io-brokernet-v1-linksspec>`
     - Links

.. _k8s-api-kanod-io-brokernet-v1-switchstatus:

SwitchStatus
------------




SwitchStatus defines the observed state of Switch

*Appears in:*


* :ref:`Switch<k8s-api-kanod-io-brokernet-v1-switch>`




.. _k8s-api-config-kanod-io-v1:

config.kanod.io/v1
==================


Package v1 contains API Schema definitions for the config v1 API group


Resource Types
--------------

- :ref:`Kanod<k8s-api-kanod-io-kanod-operator-v1-kanod>`
- :ref:`KanodList<k8s-api-kanod-io-kanod-operator-v1-kanodlist>`



.. _k8s-api-kanod-io-kanod-operator-v1-argocd:

ArgoCD
------






*Appears in:*


* :ref:`KanodSpec<k8s-api-kanod-io-kanod-operator-v1-kanodspec>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``gitcerts`` :ref:`GitCertificates<k8s-api-kanod-io-kanod-operator-v1-gitcertificates>` array
     - GitCerts is a list of certificates for git servers accessed by ArgoCD
   * - ``baremetal`` :ref:`ArgoProject<k8s-api-kanod-io-kanod-operator-v1-argoproject>`
     - Baremetal is a project description for baremetal hosts
   * - ``projects`` :ref:`ArgoProject<k8s-api-kanod-io-kanod-operator-v1-argoproject>`
     - Baremetal is a project description for cluster definitions
   * - ``network`` :ref:`ArgoProject<k8s-api-kanod-io-kanod-operator-v1-argoproject>`
     - Baremetal is a project description for network infrastructure specification
   * - ``clusterdef`` *boolean*
     - **mandatory** |br| ClusterDef controls the activation of cluster def operator
   * - ``dashboard`` *boolean*
     - **mandatory** |br| ClusterDef controls the activation of the LCM dashboard

.. _k8s-api-kanod-io-kanod-operator-v1-argoproject:

ArgoProject
-----------






*Appears in:*


* :ref:`ArgoCD<k8s-api-kanod-io-kanod-operator-v1-argocd>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``url`` *string*
     - **mandatory** |br| URL of ArgoCD project
   * - ``credentials`` *string*
     - **mandatory** |br| Name of a secret in same namespace holding the credentials

.. _k8s-api-kanod-io-kanod-operator-v1-broker:

Broker
------






*Appears in:*


* :ref:`KanodSpec<k8s-api-kanod-io-kanod-operator-v1-kanodspec>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``ip`` *string*
     - IP of the broker to propagate with CoreDNS inside the cluster This option can usually be left empty as this can be handled by a DNS server outside Kanod and there is a single domain advocated.

.. _k8s-api-kanod-io-kanod-operator-v1-brokerdef:

BrokerDef
---------






*Appears in:*


* :ref:`KanodSpec<k8s-api-kanod-io-kanod-operator-v1-kanodspec>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``credentials`` *string*
     - **mandatory** |br| Credentials is the name of secret hosting admin credenitals
   * - ``redfishDomain`` *string*
     - RedfishDomain is the domain used for exposing BMC of servers
   * - ``delegatedAuth`` *boolean*
     - DelegateAuth controls the kind of proxy activated (real proxy or delegation to user on BMC)

.. _k8s-api-kanod-io-kanod-operator-v1-brokernet:

BrokerNet
---------






*Appears in:*


* :ref:`KanodSpec<k8s-api-kanod-io-kanod-operator-v1-kanodspec>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``credentials`` *string*
     - **mandatory** |br| Credentials is the name of secret hosting admin credenitals



.. _k8s-api-kanod-io-kanod-operator-v1-gitcertificates:

GitCertificates
---------------






*Appears in:*


* :ref:`ArgoCD<k8s-api-kanod-io-kanod-operator-v1-argocd>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``host`` *string*
     - **mandatory** |br| Host name of the Git server
   * - ``value`` *string*
     - Explicit value associated as a string
   * - ``valueFrom`` :ref:`KeyRef<k8s-api-kanod-io-kanod-operator-v1-keyref>`
     - Pointer to a key in a Kubernetes resource (configmap or secret)

.. _k8s-api-kanod-io-kanod-operator-v1-ingress:

Ingress
-------






*Appears in:*


* :ref:`KanodSpec<k8s-api-kanod-io-kanod-operator-v1-kanodspec>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``ip`` *string*
     - **mandatory** |br| Ip used by the ingress (load-balancer)
   * - ``name`` *string*
     - **mandatory** |br| external name
   * - ``loadBalancerRange`` *string*
     - **mandatory** |br| Load balancer range
   * - ``key`` :ref:`KanodStringValue<k8s-api-kanod-io-kanod-operator-v1-kanodstringvalue>`
     - Key for the certificate
   * - ``certificate`` :ref:`KanodStringValue<k8s-api-kanod-io-kanod-operator-v1-kanodstringvalue>`
     - Certificate published by the ingress
   * - ``metallb`` *boolean*
     - Whether to use MetalLB or not

.. _k8s-api-kanod-io-kanod-operator-v1-ironic:

Ironic
------






*Appears in:*


* :ref:`KanodSpec<k8s-api-kanod-io-kanod-operator-v1-kanodspec>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``bmo`` *boolean*
     - **mandatory** |br| Include BareMetalOperator (default to true)
   * - ``tpm`` :ref:`Tpm<k8s-api-kanod-io-kanod-operator-v1-tpm>`
     - Whether to enable the use of TPM for server authentication
   * - ``interface`` *string*
     - **mandatory** |br| Interface used by Ironic to contact servers
   * - ``ip`` *string*
     - Ironic IP on the interface with servers
   * - ``externalIp`` *string*
     - External IP used by BareMetalHosts. Can be used to proxify Ironic
   * - ``ipaKernel`` *string*
     - Ironic Python Agent kernel URL
   * - ``ipaRamdisk`` *string*
     - Ironic Python Agent ram disk URL
   * - ``ipaKernelParams`` *string*
     - additional parameters for the IPA kernel

.. _k8s-api-kanod-io-kanod-operator-v1-kanod:

Kanod
-----




Kanod is the Schema for the kanods API

*Appears in:*


* :ref:`KanodList<k8s-api-kanod-io-kanod-operator-v1-kanodlist>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``apiVersion`` *string*
     - ``config.kanod.io/v1``
   * - ``kind`` *string*
     - ``Kanod``
   * - ``metadata`` `ObjectMeta <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/#objectmeta-v1-meta>`_
     - Refer to Kubernetes API documentation for fields of `metadata`.
   * - ``spec`` :ref:`KanodSpec<k8s-api-kanod-io-kanod-operator-v1-kanodspec>`
     - 
   * - ``status`` :ref:`KanodStatus<k8s-api-kanod-io-kanod-operator-v1-kanodstatus>`
     - 

.. _k8s-api-kanod-io-kanod-operator-v1-kanodlist:

KanodList
---------




KanodList contains a list of Kanod



.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``apiVersion`` *string*
     - ``config.kanod.io/v1``
   * - ``kind`` *string*
     - ``KanodList``
   * - ``metadata`` `ListMeta <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/#listmeta-v1-meta>`_
     - Refer to Kubernetes API documentation for fields of `metadata`.
   * - ``items`` :ref:`Kanod<k8s-api-kanod-io-kanod-operator-v1-kanod>` array
     - **mandatory** |br| 

.. _k8s-api-kanod-io-kanod-operator-v1-kanodspec:

KanodSpec
---------




KanodSpec defines the desired state of Kanod

*Appears in:*


* :ref:`Kanod<k8s-api-kanod-io-kanod-operator-v1-kanod>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``configName`` *string*
     - Name of the configmap containing the core configuration. Defaults to config
   * - ``ironic`` :ref:`Ironic<k8s-api-kanod-io-kanod-operator-v1-ironic>`
     - Ironic is the Ironic configuration
   * - ``capi`` *boolean*
     - **mandatory** |br| Capi is a boolean to optionally disable CAPI
   * - ``rke2`` *boolean*
     - **mandatory** |br| Rke2 is a boolean to optionally disable Capi Rke2 support
   * - ``certManager`` *boolean*
     - **mandatory** |br| CertManager is a boolean to optionally disable Cert-Manager (if provided elsewhere)
   * - ``externalSecret`` *boolean*
     - **mandatory** |br| ExternalSecret is a boolean to optionally disable externalSecret when Vault is used.
   * - ``brokerDef`` :ref:`BrokerDef<k8s-api-kanod-io-kanod-operator-v1-brokerdef>`
     - BrokerDef is the configuration of a redfish Broker
   * - ``brokerNet`` :ref:`BrokerNet<k8s-api-kanod-io-kanod-operator-v1-brokernet>`
     - BrokerNet is the configuration of a network broker
   * - ``argocd`` :ref:`ArgoCD<k8s-api-kanod-io-kanod-operator-v1-argocd>`
     - ArgoCD is the configuration of argocd projects
   * - ``ingress`` :ref:`Ingress<k8s-api-kanod-io-kanod-operator-v1-ingress>`
     - Ingress
   * - ``broker`` :ref:`Broker<k8s-api-kanod-io-kanod-operator-v1-broker>`
     - Broker contains the ip of the broker Redfish
   * - ``byoh`` *boolean*
     - **mandatory** |br| Byoh is a boolean to optionally enable Capi Byoh support
   * - ``kamaji`` *boolean*
     - **mandatory** |br| Kamaji is a boolean to optionally enable Capi Kamaji support

.. _k8s-api-kanod-io-kanod-operator-v1-kanodstatus:

KanodStatus
-----------




KanodStatus defines the observed state of Kanod

*Appears in:*


* :ref:`Kanod<k8s-api-kanod-io-kanod-operator-v1-kanod>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``conditions`` `Condition <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/#condition-v1-meta>`_ array
     - conditions describe the state of the built image

.. _k8s-api-kanod-io-kanod-operator-v1-kanodstringvalue:

KanodStringValue
----------------






*Appears in:*


* :ref:`GitCertificates<k8s-api-kanod-io-kanod-operator-v1-gitcertificates>`
* :ref:`Ingress<k8s-api-kanod-io-kanod-operator-v1-ingress>`
* :ref:`Tpm<k8s-api-kanod-io-kanod-operator-v1-tpm>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``value`` *string*
     - Explicit value associated as a string
   * - ``valueFrom`` :ref:`KeyRef<k8s-api-kanod-io-kanod-operator-v1-keyref>`
     - Pointer to a key in a Kubernetes resource (configmap or secret)

.. _k8s-api-kanod-io-kanod-operator-v1-keyref:

KeyRef
------






*Appears in:*


* :ref:`GitCertificates<k8s-api-kanod-io-kanod-operator-v1-gitcertificates>`
* :ref:`KanodStringValue<k8s-api-kanod-io-kanod-operator-v1-kanodstringvalue>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``configMapName`` *string*
     - Name of the configmap (exclusive with secret)
   * - ``secretName`` *string*
     - Name of the secret (exclusive with configmap)
   * - ``key`` *string*
     - Name of the key

.. _k8s-api-kanod-io-kanod-operator-v1-tpm:

Tpm
---






*Appears in:*


* :ref:`Ironic<k8s-api-kanod-io-kanod-operator-v1-ironic>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``authUrl`` *string*
     - **mandatory** |br| AuthUrl is the URL of endpoint to connect to for authentication
   * - ``authCa`` :ref:`KanodStringValue<k8s-api-kanod-io-kanod-operator-v1-kanodstringvalue>`
     - **mandatory** |br| AuthCa is the certificate used for validating the authentication URL
   * - ``registrar`` :ref:`TpmEndpoint<k8s-api-kanod-io-kanod-operator-v1-tpmendpoint>`
     - Registrar is the endpoint for the registration part.

.. _k8s-api-kanod-io-kanod-operator-v1-tpmendpoint:

TpmEndpoint
-----------






*Appears in:*


* :ref:`Tpm<k8s-api-kanod-io-kanod-operator-v1-tpm>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``ip`` *string*
     - **mandatory** |br| Ip part of the endpoint
   * - ``port`` *integer*
     - **mandatory** |br| TCP port used by the endpoint


.. _k8s-api-gitops-kanod-io-v1alpha1:

gitops.kanod.io/v1alpha1
========================


Package v1alpha1 contains API Schema definitions for the gitops v1 API group


Resource Types
--------------

- :ref:`ClusterDef<k8s-api-kanod-io-cluster-def-v1alpha1-clusterdef>`
- :ref:`ClusterDefList<k8s-api-kanod-io-cluster-def-v1alpha1-clusterdeflist>`



.. _k8s-api-kanod-io-cluster-def-v1alpha1-clusterdef:

ClusterDef
----------






*Appears in:*


* :ref:`ClusterDefList<k8s-api-kanod-io-cluster-def-v1alpha1-clusterdeflist>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``apiVersion`` *string*
     - ``gitops.kanod.io/v1alpha1``
   * - ``kind`` *string*
     - ``ClusterDef``
   * - ``metadata`` `ObjectMeta <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/#objectmeta-v1-meta>`_
     - Refer to Kubernetes API documentation for fields of `metadata`.
   * - ``spec`` :ref:`ClusterDefSpec<k8s-api-kanod-io-cluster-def-v1alpha1-clusterdefspec>`
     - 
   * - ``status`` :ref:`ClusterDefStatus<k8s-api-kanod-io-cluster-def-v1alpha1-clusterdefstatus>`
     - 

.. _k8s-api-kanod-io-cluster-def-v1alpha1-clusterdeflist:

ClusterDefList
--------------




ClusterDefList contains a list of ClusterDef



.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``apiVersion`` *string*
     - ``gitops.kanod.io/v1alpha1``
   * - ``kind`` *string*
     - ``ClusterDefList``
   * - ``metadata`` `ListMeta <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/#listmeta-v1-meta>`_
     - Refer to Kubernetes API documentation for fields of `metadata`.
   * - ``items`` :ref:`ClusterDef<k8s-api-kanod-io-cluster-def-v1alpha1-clusterdef>` array
     - **mandatory** |br| 

.. _k8s-api-kanod-io-cluster-def-v1alpha1-clusterdefspec:

ClusterDefSpec
--------------




ClusterDefSpec defines the desired state of ClusterDef

*Appears in:*


* :ref:`ClusterDef<k8s-api-kanod-io-cluster-def-v1alpha1-clusterdef>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``source`` :ref:`GitLocation<k8s-api-kanod-io-cluster-def-v1alpha1-gitlocation>`
     - **mandatory** |br| Source is the location of the cluster definition.
   * - ``credentialName`` *string*
     - **mandatory** |br| CredentialName is the user credentials for accessing git repository It must be in a secret in the same namespace
   * - ``configurationName`` *string*
     - **mandatory** |br| ConfigurationName is the configuration supplied by the infrastructure administrator to render the specification of the cluster definition It is a configmap in the same namespace
   * - ``cidataName`` *string*
     - CidataName is the name of the configmap that customize the cluster cloud-init datas. This has precedence over all other sources of cloud-init data.
   * - ``pivotInfo`` :ref:`PivotInformation<k8s-api-kanod-io-cluster-def-v1alpha1-pivotinformation>`
     - PivotInfo contains information needed for the pivoting of the management functions from the management cluster to the workload cluster.
   * - ``multiTenant`` *boolean*
     - MultiTenant is a boolean that indicates if the cluster namespace is managed in multitenant mode. If MultiTenant = false : the target cluster namespace is the same as the clusterdef resource. If MultiTenant = true : the target cluster namespace is the concatenation of name/namespace. of the clusterdef resource
   * - ``poolUserList`` map [*string*] :ref:`PoolUserSpec<k8s-api-kanod-io-cluster-def-v1alpha1-pooluserspec>`
     - PoolUserList contains information to access the brokerdef for a pool user
   * - ``networkList`` map [*string*] :ref:`NetworkSpec<k8s-api-kanod-io-cluster-def-v1alpha1-networkspec>`
     - NetworkList contains information to access the brokernet for a network definition
   * - ``createArgocdClusterSecret`` *boolean*
     - CreateArgoCDSecret indicates if the argoCD secret associated with the target cluster should be created

.. _k8s-api-kanod-io-cluster-def-v1alpha1-clusterdefstate:

ClusterDefState
---------------


*Underlying type:* *string*

ClusterDefState describes the possible phase of the cluster definition

*Appears in:*


* :ref:`ClusterDefStatus<k8s-api-kanod-io-cluster-def-v1alpha1-clusterdefstatus>`



.. _k8s-api-kanod-io-cluster-def-v1alpha1-clusterdefstatus:

ClusterDefStatus
----------------




ClusterDefStatus defines the observed state of ClusterDef

*Appears in:*


* :ref:`ClusterDef<k8s-api-kanod-io-cluster-def-v1alpha1-clusterdef>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``pivotstatus`` :ref:`PivotState<k8s-api-kanod-io-cluster-def-v1alpha1-pivotstate>`
     - **mandatory** |br| Status of pivot
   * - ``phase`` :ref:`ClusterDefState<k8s-api-kanod-io-cluster-def-v1alpha1-clusterdefstate>`
     - **mandatory** |br| Status of deployment
   * - ``controlPlaneVersion`` *string*
     - **mandatory** |br| ControlPlaneVersion is the version supported by control plane
   * - ``machineVersions`` *string* array
     - **mandatory** |br| MachineVersions is a sorted array of different versions of the kubelet deployed on machines

.. _k8s-api-kanod-io-cluster-def-v1alpha1-gitlocation:

GitLocation
-----------




GitLocation defines a complete path to a version of a folder on a git repository

*Appears in:*


* :ref:`ClusterDefSpec<k8s-api-kanod-io-cluster-def-v1alpha1-clusterdefspec>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``repository`` *string*
     - **mandatory** |br| Repository is the URL of the project hosting the files
   * - ``branch`` *string*
     - Branch is the git branch followed by ClusterDef
   * - ``path`` *string*
     - Path is the subpath of the folder hosting the configuration file.

.. _k8s-api-kanod-io-cluster-def-v1alpha1-networkspec:

NetworkSpec
-----------




NetworkSpec contains information for interacting with a network definition

*Appears in:*


* :ref:`ClusterDefSpec<k8s-api-kanod-io-cluster-def-v1alpha1-clusterdefspec>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``networkdefName`` *string*
     - **mandatory** |br| NetworkdefName is the name of the network definition
   * - ``address`` *string*
     - **mandatory** |br| Address is the address of the brokernet
   * - ``brokerConfig`` *string*
     - **mandatory** |br| BrokerConfig is the name of the configmap containing the brokernet CA
   * - ``brokerCredentials`` *string*
     - **mandatory** |br| BrokerCredentials is the name of the secret containing the credentials for the brokernet

.. _k8s-api-kanod-io-cluster-def-v1alpha1-pivotinformation:

PivotInformation
----------------




Pivot defines informations nedded for pivoting

*Appears in:*


* :ref:`ClusterDefSpec<k8s-api-kanod-io-cluster-def-v1alpha1-clusterdefspec>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``pivot`` *boolean*
     - **mandatory** |br| Pivot indicates if the cluster pivoting process should be launched
   * - ``bareMetalPoolList`` *string* array
     - BareMetalPoolList contains the list of BareMetalPool custom resources to be moved on the target cluster
   * - ``networkList`` *string* array
     - NetworkList contains the list of Network custom resources to be moved on the target cluster
   * - ``kanodName`` *string*
     - **mandatory** |br| Kanod operator custom resource name
   * - ``kanodNamespace`` *string*
     - **mandatory** |br| namespace of the Kanod operator custom resource
   * - ``ironicItf`` *string*
     - **mandatory** |br| interface used by ironic

.. _k8s-api-kanod-io-cluster-def-v1alpha1-pivotstate:

PivotState
----------


*Underlying type:* *string*

PivotState describes the possible phase of the cluster pivoting process

*Appears in:*


* :ref:`ClusterDefStatus<k8s-api-kanod-io-cluster-def-v1alpha1-clusterdefstatus>`



.. _k8s-api-kanod-io-cluster-def-v1alpha1-pooluserspec:

PoolUserSpec
------------




PoolUser contains information for interacting with a poolUser

*Appears in:*


* :ref:`ClusterDefSpec<k8s-api-kanod-io-cluster-def-v1alpha1-clusterdefspec>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``username`` *string*
     - **mandatory** |br| Username of the pool user
   * - ``address`` *string*
     - **mandatory** |br| Address is the address of the broker
   * - ``brokerConfig`` *string*
     - **mandatory** |br| BrokerConfig is the name of the configmap containing the broker CA
   * - ``brokerCredentials`` *string*
     - **mandatory** |br| BrokerCredentials is the name of the secret containing the credentials for the broker


.. _k8s-api-netpool-kanod-io-v1:

netpool.kanod.io/v1
===================


Package v1 contains API Schema definitions for the netpool v1 API group


Resource Types
--------------

- :ref:`Network<k8s-api-kanod-io-network-operator-v1-network>`
- :ref:`NetworkList<k8s-api-kanod-io-network-operator-v1-networklist>`



.. _k8s-api-kanod-io-network-operator-v1-baremetalpoolbinding:

BareMetalPoolBinding
--------------------




BareMetalPoolBinding defines a link between a BareMetalPool and the network.

*Appears in:*


* :ref:`NetworkSpec<k8s-api-kanod-io-network-operator-v1-networkspec>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``name`` *string*
     - **mandatory** |br| Name of the baremetal pool
   * - ``bindingName`` *string*
     - **mandatory** |br| Name of the binding used in labels on baremetalhost

.. _k8s-api-kanod-io-network-operator-v1-dhcpconfig:

DhcpConfig
----------




DhcpConfig contains the dhcp params associated with the networkdefinition

*Appears in:*


* :ref:`NetworkStatus<k8s-api-kanod-io-network-operator-v1-networkstatus>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``subnetPrefix`` *string*
     - **mandatory** |br| Subnet IP/mask address
   * - ``subnetStart`` *string*
     - **mandatory** |br| Subnet range start
   * - ``subnetEnd`` *string*
     - **mandatory** |br| Subnet range end
   * - ``dhcpMode`` :ref:`DhcpMode<k8s-api-kanod-io-network-operator-v1-dhcpmode>`
     - **mandatory** |br| Dhcp mode
   * - ``domainNameServers`` *string* array
     - DomainNameServers
   * - ``gatewayIp`` *string*
     - Gateway IP/mask address
   * - ``dhcpServerIp`` *string*
     - **mandatory** |br| Subnet DHCP server address

.. _k8s-api-kanod-io-network-operator-v1-dhcpmode:

DhcpMode
--------


*Underlying type:* *string*



*Appears in:*


* :ref:`DhcpConfig<k8s-api-kanod-io-network-operator-v1-dhcpconfig>`



.. _k8s-api-kanod-io-network-operator-v1-network:

Network
-------




Network is the Schema for the networks API

*Appears in:*


* :ref:`NetworkList<k8s-api-kanod-io-network-operator-v1-networklist>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``apiVersion`` *string*
     - ``netpool.kanod.io/v1``
   * - ``kind`` *string*
     - ``Network``
   * - ``metadata`` `ObjectMeta <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/#objectmeta-v1-meta>`_
     - Refer to Kubernetes API documentation for fields of `metadata`.
   * - ``spec`` :ref:`NetworkSpec<k8s-api-kanod-io-network-operator-v1-networkspec>`
     - 
   * - ``status`` :ref:`NetworkStatus<k8s-api-kanod-io-network-operator-v1-networkstatus>`
     - 

.. _k8s-api-kanod-io-network-operator-v1-networklist:

NetworkList
-----------




NetworkList contains a list of Network



.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``apiVersion`` *string*
     - ``netpool.kanod.io/v1``
   * - ``kind`` *string*
     - ``NetworkList``
   * - ``metadata`` `ListMeta <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/#listmeta-v1-meta>`_
     - Refer to Kubernetes API documentation for fields of `metadata`.
   * - ``items`` :ref:`Network<k8s-api-kanod-io-network-operator-v1-network>` array
     - **mandatory** |br| 

.. _k8s-api-kanod-io-network-operator-v1-networkspec:

NetworkSpec
-----------




NetworkSpec defines the desired state of Network

*Appears in:*


* :ref:`Network<k8s-api-kanod-io-network-operator-v1-network>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``networkDefinitionName`` *string*
     - NetworkDefinition name
   * - ``bareMetalPoolList`` :ref:`BareMetalPoolBinding<k8s-api-kanod-io-network-operator-v1-baremetalpoolbinding>` array
     - Pools list
   * - ``networkDefinitionCredentialName`` *string*
     - **mandatory** |br| Name of the secret associated to the NetworkDefinitionName
   * - ``brokernetAddress`` *string*
     - **mandatory** |br| Address of the brokernet api server
   * - ``brokerConfig`` *string*
     - **mandatory** |br| BrokerConfig is the name of the configmap containing the broker CA
   * - ``dhcpServerIp`` *string*
     - DhcpServerIP associated with the NetworkDefinitionName
   * - ``ipPool`` *string*
     - IPPool is the name of an optional IPPool that will be associated to the network
   * - ``cluster`` *string*
     - Cluster is the name of the cluster for which the network is defined. This optional parameter is only necessary for pivot when an IPPool is defined.

.. _k8s-api-kanod-io-network-operator-v1-networkstatus:

NetworkStatus
-------------




NetworkStatus defines the observed state of Network

*Appears in:*


* :ref:`Network<k8s-api-kanod-io-network-operator-v1-network>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``state`` *string*
     - **mandatory** |br| Status
   * - ``pools`` *string*
     - **mandatory** |br| 
   * - ``dhcpConfig`` :ref:`DhcpConfig<k8s-api-kanod-io-network-operator-v1-dhcpconfig>`
     - **mandatory** |br| 


.. _k8s-api-network-kanod-io-v1alpha1:

network.kanod.io/v1alpha1
=========================


Package v1alpha1 contains API Schema definitions for the network v1alpha1 API group


Resource Types
--------------

- :ref:`DhcpConfig<k8s-api-kanod-io-kanod-kea-v1alpha1-dhcpconfig>`
- :ref:`DhcpConfigList<k8s-api-kanod-io-kanod-kea-v1alpha1-dhcpconfiglist>`



.. _k8s-api-kanod-io-kanod-kea-v1alpha1-dhcpconfig:

DhcpConfig
----------




DhcpConfig is the Schema for the dhcpconfigs API

*Appears in:*


* :ref:`DhcpConfigList<k8s-api-kanod-io-kanod-kea-v1alpha1-dhcpconfiglist>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``apiVersion`` *string*
     - ``network.kanod.io/v1alpha1``
   * - ``kind`` *string*
     - ``DhcpConfig``
   * - ``metadata`` `ObjectMeta <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/#objectmeta-v1-meta>`_
     - Refer to Kubernetes API documentation for fields of `metadata`.
   * - ``spec`` :ref:`DhcpConfigSpec<k8s-api-kanod-io-kanod-kea-v1alpha1-dhcpconfigspec>`
     - 
   * - ``status`` :ref:`DhcpConfigStatus<k8s-api-kanod-io-kanod-kea-v1alpha1-dhcpconfigstatus>`
     - 

.. _k8s-api-kanod-io-kanod-kea-v1alpha1-dhcpconfiglist:

DhcpConfigList
--------------




DhcpConfigList contains a list of DhcpConfig



.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``apiVersion`` *string*
     - ``network.kanod.io/v1alpha1``
   * - ``kind`` *string*
     - ``DhcpConfigList``
   * - ``metadata`` `ListMeta <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/#listmeta-v1-meta>`_
     - Refer to Kubernetes API documentation for fields of `metadata`.
   * - ``items`` :ref:`DhcpConfig<k8s-api-kanod-io-kanod-kea-v1alpha1-dhcpconfig>` array
     - **mandatory** |br| 

.. _k8s-api-kanod-io-kanod-kea-v1alpha1-dhcpconfigspec:

DhcpConfigSpec
--------------




DhcpConfigSpec defines the desired state of DhcpConfig

*Appears in:*


* :ref:`DhcpConfig<k8s-api-kanod-io-kanod-kea-v1alpha1-dhcpconfig>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``subnet`` *string*
     - **mandatory** |br| Prefix of the network
   * - ``prefix`` *integer*
     - Mask size
   * - ``pools`` :ref:`Pool<k8s-api-kanod-io-kanod-kea-v1alpha1-pool>` array
     - Pools used in that subnet
   * - ``pxe`` *boolean*
     - Whether PXE boot is enabled on the network
   * - ``gateway`` *string*
     - Gateway address advertised by DHCP server
   * - ``dnsServers`` *string* array
     - List of DNS server addresses advertised by DHCP server
   * - ``ipPool`` *string*
     - IPPool to generate or omit if not used

.. _k8s-api-kanod-io-kanod-kea-v1alpha1-dhcpconfigstatus:

DhcpConfigStatus
----------------




DhcpConfigStatus defines the observed state of DhcpConfig

*Appears in:*


* :ref:`DhcpConfig<k8s-api-kanod-io-kanod-kea-v1alpha1-dhcpconfig>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``subnetId`` *integer*
     - Kea id of the subnet.

.. _k8s-api-kanod-io-kanod-kea-v1alpha1-pool:

Pool
----




Pool define a pool of available addresses within a subnet.

*Appears in:*


* :ref:`DhcpConfigSpec<k8s-api-kanod-io-kanod-kea-v1alpha1-dhcpconfigspec>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``start`` *string*
     - Range start for address allocation (single range)
   * - ``end`` *string*
     - Range end for address allocation (single range)


.. _k8s-api-network-kanod-io-v1beta1:

network.kanod.io/v1beta1
========================


Package v1beta1 contains API Schema definitions for the network v1beta1 API group


Resource Types
--------------

- :ref:`BmhData<k8s-api-kanod-io-bmh-data-v1beta1-bmhdata>`
- :ref:`BmhDataList<k8s-api-kanod-io-bmh-data-v1beta1-bmhdatalist>`



.. _k8s-api-kanod-io-bmh-data-v1beta1-bmhdata:

BmhData
-------




BmhData is the Schema for the bmhdatas API

*Appears in:*


* :ref:`BmhDataList<k8s-api-kanod-io-bmh-data-v1beta1-bmhdatalist>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``apiVersion`` *string*
     - ``network.kanod.io/v1beta1``
   * - ``kind`` *string*
     - ``BmhData``
   * - ``metadata`` `ObjectMeta <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/#objectmeta-v1-meta>`_
     - Refer to Kubernetes API documentation for fields of `metadata`.
   * - ``spec`` :ref:`BmhDataSpec<k8s-api-kanod-io-bmh-data-v1beta1-bmhdataspec>`
     - 
   * - ``status`` :ref:`BmhDataStatus<k8s-api-kanod-io-bmh-data-v1beta1-bmhdatastatus>`
     - 

.. _k8s-api-kanod-io-bmh-data-v1beta1-bmhdatalist:

BmhDataList
-----------




BmhDataList contains a list of BmhData



.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``apiVersion`` *string*
     - ``network.kanod.io/v1beta1``
   * - ``kind`` *string*
     - ``BmhDataList``
   * - ``metadata`` `ListMeta <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/#listmeta-v1-meta>`_
     - Refer to Kubernetes API documentation for fields of `metadata`.
   * - ``items`` :ref:`BmhData<k8s-api-kanod-io-bmh-data-v1beta1-bmhdata>` array
     - **mandatory** |br| 

.. _k8s-api-kanod-io-bmh-data-v1beta1-bmhdataspec:

BmhDataSpec
-----------




BmhDataSpec defines the desired state of BmhData

*Appears in:*


* :ref:`BmhData<k8s-api-kanod-io-bmh-data-v1beta1-bmhdata>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``preprovisioningNetwork`` *string*
     - PreprovisioningNetworkTemplate is the name of a secret containing a go template definition of the preprovisioning network configuration in Glean format at the key template
   * - ``networkData`` *string*
     - NetworkData is the name of an optional secret containing a go template definition of the network data of the host.
   * - ``userData`` *string*
     - UserData is the name of an optional secret containing a go template definition of the user data of the host.
   * - ``generateMetadata`` *boolean*
     - GenerateMetadata indicates which secrets to generate and link
   * - ``ipAddressesFromIPPool`` :ref:`FromIPPool<k8s-api-kanod-io-bmh-data-v1beta1-fromippool>` array
     - IpAddressesFromIPPool is a list of addresses generated for each instance of the template.
   * - ``prefixesFromIPPool`` :ref:`FromIPPool<k8s-api-kanod-io-bmh-data-v1beta1-fromippool>` array
     - PrefixesFromIPPool retrieves the prefix of the ippools
   * - ``gatewaysFromIPPool`` :ref:`FromIPPool<k8s-api-kanod-io-bmh-data-v1beta1-fromippool>` array
     - GatewaysFromIPPool retrieves gateway associated to ippools
   * - ``dnsServersFromIPPool`` :ref:`FromIPPool<k8s-api-kanod-io-bmh-data-v1beta1-fromippool>` array
     - DnsServersFromIPPool retrieves dns associated to ippools
   * - ``fromLabels`` :ref:`FromLabel<k8s-api-kanod-io-bmh-data-v1beta1-fromlabel>` array
     - FromLabels associate keys to values of some labels in the BaremetalHost
   * - ``fromAnnotations`` :ref:`FromAnnotation<k8s-api-kanod-io-bmh-data-v1beta1-fromannotation>` array
     - FromLabels associate keys to values of some labels in the BaremetalHost
   * - ``bootstrapConfig`` :ref:`ByohConfig<k8s-api-kanod-io-bmh-data-v1beta1-byohconfig>`
     - ByohConfig is a descriptor for a Kubernetes cluster with the BYOH provider

.. _k8s-api-kanod-io-bmh-data-v1beta1-bmhdatastatus:

BmhDataStatus
-------------




BmhDataStatus defines the observed state of BmhData

*Appears in:*


* :ref:`BmhData<k8s-api-kanod-io-bmh-data-v1beta1-bmhdata>`



.. _k8s-api-kanod-io-bmh-data-v1beta1-byohconfig:

ByohConfig
----------






*Appears in:*


* :ref:`BmhDataSpec<k8s-api-kanod-io-bmh-data-v1beta1-bmhdataspec>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``apiServer`` *string*
     - **mandatory** |br| 
   * - ``insecureSkipTLSVerify`` *boolean*
     - 

.. _k8s-api-kanod-io-bmh-data-v1beta1-fromannotation:

FromAnnotation
--------------






*Appears in:*


* :ref:`BmhDataSpec<k8s-api-kanod-io-bmh-data-v1beta1-bmhdataspec>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``key`` *string*
     - **mandatory** |br| Key is the name of the element in the templates or in meta-data
   * - ``annotation`` *string*
     - **mandatory** |br| Annotation is the name of the label on the BareMetalHost

.. _k8s-api-kanod-io-bmh-data-v1beta1-fromippool:

FromIPPool
----------






*Appears in:*


* :ref:`BmhDataSpec<k8s-api-kanod-io-bmh-data-v1beta1-bmhdataspec>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``key`` *string*
     - **mandatory** |br| Key is the key of the element in the templates or in meta-data
   * - ``name`` *string*
     - **mandatory** |br| Name of the IPPool used
   * - ``apiGroup`` *string*
     - **mandatory** |br| Group of the IPPool
   * - ``kind`` *string*
     - **mandatory** |br| Kind of the IPPool

.. _k8s-api-kanod-io-bmh-data-v1beta1-fromlabel:

FromLabel
---------






*Appears in:*


* :ref:`BmhDataSpec<k8s-api-kanod-io-bmh-data-v1beta1-bmhdataspec>`

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Field
     - Description
   * - ``key`` *string*
     - **mandatory** |br| Key is the name of the element in the templates or in meta-data
   * - ``label`` *string*
     - **mandatory** |br| Label is the name of the label on the BareMetalHost

