.. kanod documentation master file, created by
   sphinx-quickstart on Wed Jul 14 23:54:34 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Kanod
=====

Kanod is a framework to manage the life-cycle of Baremetal Kubernetes Clusters.
It uses a standard state-of-the art approach to Kubernetes Life-cycle management
based on :ref:`Cluster-Api<clusterapi>`
and :ref:`Gitops<gitops>` but it adds some new features that help the
day-to-day management of multiple Kubernetes clusters on baremetal suitable
for Telco deployments:

.. grid:: 3

   .. grid-item-card:: Baremetal as a Service Approach

      We propose two approaches to simplify multi-tenancy with bare-metal servers:

      * We can fully virtualize a server through its :ref:`BMC<bmaas>`. The
        :ref:`BareMetalPool<baremetalpool>` custom resource represents a set of
        leased servers fulfilling a set of hardware characteristics.
      * We can also virtualize just the notion of :ref:`Compute resource<compute_aas>`.
        A :ref:`Host<host_resource>` gives access to a compute resource
        for launching workloads but without control of the underlying hardware.

   .. grid-item-card:: :ref:`Fully Declarative OS Management<decl-os>`

      We introduce a framework to make cloud-init extensible and
      ensure that the initialization of the underlying OS is fully declarative.
      We use this framework to improve the security of the infrastructure: servers
      have a secured access to a secure vault.

   .. grid-item-card:: :ref:`High Level Synthesis of Cluster Content<synthesis>`

      Defining a clusters means combining information about the customer (cluster
      administrator) needs and the infrastructure administrator knowledge and
      requirements (gateway locations, networking constraints, etc.) The updater
      is an argocd plugin that combines two sources of information to build the
      cluster description.

.. card:: :doc:`Kanod in a Bottle <kiab/index>`

   *Kanod in a bottle* is a set of scripts that deploys different usage
   scenarios of Kanod on a single machine using virtual machines to emulate servers with
   either IPMI or Redfish BMC API.


.. note::
   Kanod is a research project that explores how Kubernetes could be tomorow
   Network OS managing fleets of cooperating clusters with well defined and
   secured interfaces. It is not an industrial grade offering.

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Contents:

   architecture/index
   kiab/index

   administration

   machines/index

   pools/index

   hosts/index

   tools

   api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
