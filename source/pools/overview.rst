=====================================
Overview of Multitenancy architecture
=====================================

Cluster-API is designed in such a way that each cluster is independent
from the others and can safely reside in its own namespace with operators
handling its resources.

With this approach, pivoting the cluster (transfering the control from an
ephemeral management cluster to the deployed cluster itself) is just roughly
moving the content from a namespace to another cluster.

Unfortunately this approach falls short with baremetal servers unless they
are pre-allocated to a given cluster which limit the dynamicity/elasticity
of the solution.

Kanod approach is to distinguish the raw server resource from the lightly
virtualized one given to clusters. The virtualization is done through the
access to the BMC :

* either through a proxy
* or using accounts with limited control on user management on the BMC

Cluster namespaces need now an API to request new baremetal servers from
the infrastructure. This API is the **baremetalpool** custom resource.

Behind the scene, baremetalpools are linked to counterparts (**brokerdef** broker) in the
infrastructure that protects the access to the real resources and assign them
to the various pools.

The configuration of the network for the workload cluster leverages the same principle.
A **Network** custom resources offers an API to link a baremetalpool to a virtual network.

This Network resource is backed by the **brokernet** broker in the infrastructure
that manages the creation of the virtual network and the attachment
of the real server resources to this network.

The following figure describes the spreading of the resources between the LCM cluster
and the different infrastructures:

.. figure:: urba.png
