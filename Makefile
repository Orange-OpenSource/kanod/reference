# Minimal makefile for Sphinx documentation
#

# You can set these variables from the command line.
SPHINXOPTS    =
SPHINXBUILD   = sphinx-build
SOURCEDIR     = source
BUILDDIR      = build

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

pre-clean:
	rm -rf source/kiab
	rm -rf source/launch
	rm -rf source/clusterdef
	rm -rf source/dashboard
	rm -rf source/registrar
	rm -rf source/image-builder
	rm -rf source/updater
	rm -rf source/machines/core
	rm -rf source/machines/lcm
	rm -rf source/pools/baremetalpool
	rm -rf source/pools/brokerdef
	rm -rf source/pools/network-operator
	rm -rf source/pools/brokernet
	rm -rf source/hosts/host-operator
	rm -rf source/hosts/host-kubevirt-operator
	rm -rf source/hosts/host-openstack-operator
	rm -rf source/hosts/host-remote-operator
	rm -rf source/hosts/host-quota

pre-build: pre-clean
	cp -r import/kanod-in-a-bottle/doc/source source/kiab
	make -C import/kanod-boot/doc pre-build
	cp -r import/kanod-boot/doc/source source/launch
	make -C import/kanod-updater/doc pre-build
	cp -r import/kanod-updater/doc/source source/updater

	make -C import/cluster-def/doc pre-build
	cp -r import/cluster-def/doc/source source/clusterdef
	make -C import/kanod-node/doc pre-build
	cp -r import/kanod-node/doc/source source/machines/lcm
	cp -r import/dashboard/doc/source source/dashboard
	make -C import/image-builder/doc pre-build
	cp -r import/image-builder/doc/source/core source/machines/core
	cp -r import/image-builder/doc/source/image-builder source/image-builder
	cp -r import/baremetalpool/doc/source source/pools/baremetalpool
	cp -r import/brokerdef/doc/source source/pools/brokerdef
	cp -r import/network-operator/doc/source source/pools/network-operator
	cp -r import/brokernet/doc/source source/pools/brokernet
	make -C import/registrar/doc pre-build
	cp -r import/registrar/doc/source source/registrar
	make -C import/host-operator/doc pre-build
	cp -r import/host-operator/doc/source source/hosts/host-operator
	cp -r import/host-openstack-operator/doc/source source/hosts/host-openstack-operator
	cp -r import/host-kubevirt-operator/doc/source source/hosts/host-kubevirt-operator
	cp -r import/host-remote-operator/doc/source source/hosts/host-remote-operator
	cp -r import/host-quota/doc/source source/hosts/host-quota

.ONESHELL:
sync:
	BRANCH=master
	for e in import/*; do 
		(cd $$e; 
		pwd
		br=$$(git branch -r -l origin/main); 
		if [ ! -z "$${br}" ];  then BRANCH=main; fi; 
		echo "branch : $$BRANCH";  git checkout $$BRANCH; git pull origin $$BRANCH); 
	done

.PHONY: help Makefile

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)
